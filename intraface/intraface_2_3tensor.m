function [points_three_tensor,angles,missed_frame_idxs] = intraface_2_3tensor(points_structure)

% points_dim=size(points_structure{1}.pred);
% 
% points_three_tensor = zeros(points_dim(1),points_dim(2),length(points_structure));
% angles = zeros(length(points_structure),3);

% for i = 1:length(points_structure)
% 
%     if(~isempty(points_structure{i}.pred))
%         points_three_tensor(:,:,i) = points_structure{i}.pred;
%         angles(i,:)=points_structure{i}.pose.angle;
%     end
% end

points_three_tensor=[];
angles=[];
missed_frame_idxs=[];

for i = 1:length(points_structure)

    if(~isempty(points_structure{i}.pred))
        points_three_tensor = cat(3,points_three_tensor,points_structure{i}.pred);
        angles = [angles;points_structure{i}.pose.angle];
    else
        points_three_tensor = cat(3,points_three_tensor,nan*ones(49,2));
        angles = [angles;nan*ones(1,3)];
        missed_frame_idxs = [missed_frame_idxs; i];
    end
end
missed_frame_idxs
if(~isempty(find(diff(missed_frame_idxs>1))))
    warning('Intraface missed many frames...');
end