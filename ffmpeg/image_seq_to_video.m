function output_file = image_seq_to_video(framerate,img_regex,output_file, async )



if(~exist('async','var'))
    async=0;
end

if(async)
    cmd_tail = '&';
else
    cmd_tail = '';
end



curr_folder=dirname(mfilename('fullpath'));

cmd_parts = {path_join(curr_folder,'image_seq_to_video.sh'),my_num_to_str(framerate),img_regex,output_file, cmd_tail};

cmd = build_string_args(cmd_parts);

system(cmd);