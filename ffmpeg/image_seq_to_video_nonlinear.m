function output_file = image_seq_to_video_nonlinear(framerate,img_dir,hold_idxs,output_file )

img_regex=path_join(img_dir,'%d.png');

curr_folder=dirname(mfilename('fullpath'));



cmd_parts = {path_join(curr_folder,'image_seq_to_video.sh'),my_num_to_str(framerate),img_regex,output_file};

cmd = build_string_args(cmd_parts);

system(cmd);