function video_num_frames=get_video_length(video_filepath,impl)
    
if(exist('impl','var') && strcmp(impl,'ffprobe'))

    length_cmd = ['ffprobe -i $(readlink -f ' video_filepath ') -show_entries format=duration -v quiet -of csv="p=0"'];
    frame_rate_cmd = ['ffprobe $(readlink -f ' video_filepath ') 2>&1| grep ",* fps" | cut -d "," -f 5 | cut -d " " -f 2'];
    [~,duration] = system(length_cmd);
    [~,frame_rate] = system(frame_rate_cmd);

    video_num_frames = round(round(str2num(frame_rate))*str2num(duration));
elseif(exist('impl','var') && strcmp(impl,'text'))
    text_video_filepath = [video_filepath '.txt'];
    metadata_map = kv_read(text_video_filepath);
    frame_rate = my_str_to_num(kv_get('frame_rate',metadata_map));
    duration = my_str_to_num(kv_get('duration',metadata_map));
    video_num_frames = round(frame_rate*duration);
else

    vr = VideoReader(video_filepath);
    video_num_frames = vr.NumberOfFrames;
    
end
