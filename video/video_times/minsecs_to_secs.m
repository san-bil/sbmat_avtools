function [ secs ] = minsecs_to_secs( minsecs )


% in: a 2 column numeric matrix containing time values as (mins_1, secs_1; mins_2, secs_2; mins_3, secs_3;...) etc
%
% out: a numeric matrix containing seconds
%
% desc: Given a 2-column array of time measurements, where the left column 
% stores minutes, and the right column stores seconds values, returns an vector of 
% time values in seconds
%
% tags: #times #videos #timestamps

secs = (minsecs(:,1)*60) + minsecs(:,2);

end

